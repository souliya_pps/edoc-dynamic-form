import { Route as rootRoute } from "./routes/__root"
import { Route as ToastRoute } from "./routes/toast"
import { Route as MuiTiptapRoute } from "./routes/muiTiptap"
import { Route as EditorRoute } from "./routes/editor"

declare module "@tanstack/react-router" {
    interface FileRoutesByPath {
        "/toast": {
            parentRoute: typeof rootRoute
        },
        "/muiTiptap": {
            parentRoute: typeof rootRoute
        },
        "/editor": {
            parentRoute: typeof rootRoute
        }
    }
}

Object.assign(ToastRoute.options, {
    path: "/toast",
    getParentRoute: () => rootRoute,
})

Object.assign(MuiTiptapRoute.options, {
    path: "/muiTiptap",
    getParentRoute: () => rootRoute,
})

Object.assign(EditorRoute.options, {
    path: "/editor",
    getParentRoute: () => rootRoute,
})

export const routeTree = rootRoute.addChildren([ToastRoute, MuiTiptapRoute, EditorRoute])