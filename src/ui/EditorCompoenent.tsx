import Attaches from "@editorjs/attaches";
import Checklist from "@editorjs/checklist";
import EditorJS from "@editorjs/editorjs";
import Header from "@editorjs/header";
import Image from "@editorjs/image";
import Table from "editorjs-table";
import { useEffect, useRef } from "react";

const DEFAULT_INITIAL_DATA = {
  time: new Date().getTime(),
  blocks: [
    {
      type: "header",
      data: {
        text: "Permit To Work",
        level: 2,
      },
    },
    {
      type: "image",
      data: {
        url: "https://i.ibb.co/7XvdyVp/512.png", // Replace with the actual image URL
        caption: "Image Caption",
        withBorder: true,
        stretched: false,
        withBackground: false,
        link: {
          url: "https://i.ibb.co/7XvdyVp/512.png", // Replace with the actual link URL
          target: "_blank", // Optional: Specify the target attribute
        },
      },
    },
    {
      type: "table",
      data: {
        content: [["Header 1", "Header 2", "Header 3"], ["Row 1"], ["Row 2"]],
      },
    },
    {
      type: "checklist",
      data: {
        items: [
          { text: "Item 1", checked: true },
          { text: "Item 2", checked: false },
          { text: "Item 3", checked: true },
        ],
      },
    },
  ],
};

const EditorComponent = () => {
  const ejInstance = useRef<EditorJS>(null);

  const initEditor = () => {
    const editor = new EditorJS({
      holder: "editorjs",
      onReady: () => {
        ejInstance.current = editor;
      },
      autofocus: true,
      data: DEFAULT_INITIAL_DATA,
      onChange: async () => {
        let content = await editor.saver.save();

        console.log(content);
      },
      tools: {
        header: Header,
        table: {
          class: Table,
          inlineToolbar: true,
          config: {
            rows: 2,
            cols: 3,
          },
        },
        checklist: {
          class: Checklist,
          inlineToolbar: true,
          config: {
            itemStyle: {
              textAlign: "center", // Align checklist items to the center
            },
          },
        },
        image: {
          class: Image,
          config: {
            endpoints: {
              byFile: "http://localhost:3000/api/upload", // Your backend file uploader endpoint
              byUrl: "http://localhost:3000/api/fetchUrl", // Your endpoint that provides uploading by Url
            },
          },
        },
        attaches: {
          class: Attaches,
          config: {
            endpoint: "https://api.imgbb.com/1/upload",
            fieldName: "file",
          },
        },
      },
    });
  };

  // This will run only once
  useEffect(() => {
    if (ejInstance.current === null) {
      initEditor();
    }

    return () => {
      ejInstance?.current?.destroy();
      ejInstance.current = null;
    };
  }, [ejInstance]);

  return (
    <>
      <div id="editorjs" />
    </>
  );
};

export default EditorComponent;
