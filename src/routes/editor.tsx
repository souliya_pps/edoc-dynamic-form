import { FileRoute } from "@tanstack/react-router";
import EditorComponent from "../ui/EditorCompoenent";

// Route Registration
export const Route = new FileRoute("/editor").createRoute({
  component: Editor,
});

function Editor() {
  return (
    <>
      <EditorComponent />
    </>
  );
}
