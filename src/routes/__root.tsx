import { Link, Outlet, RootRoute } from "@tanstack/react-router";
import { TanStackRouterDevtools } from "@tanstack/router-devtools";

export const Route = new RootRoute({
  component: RootComponent,
});

function RootComponent() {
  return (
    <>
      <div className="p-2 flex gap-2 text-lg">
        <Link
          to="/toast"
          activeProps={{
            className: "font-bold",
          }}
          activeOptions={{ exact: true }}
        >
          Toast-UI
        </Link>
        &nbsp; &nbsp;
        <Link
          to="/muiTiptap"
          activeProps={{
            className: "font-bold",
          }}
          activeOptions={{ exact: true }}
        >
          MUI-Tiptap
        </Link>
        &nbsp; &nbsp;
        <Link
          to="/editor"
          activeProps={{
            className: "font-bold",
          }}
          activeOptions={{ exact: true }}
        >
          Editor.JS
        </Link>
      </div>
      <hr />
      <Outlet />
      {/* optional, for debugging */}
      <TanStackRouterDevtools position="bottom-right" />
    </>
  );
}
