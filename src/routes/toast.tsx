import { FileRoute } from "@tanstack/react-router";
import html2canvas from "html2canvas";
import jsPDF from "jspdf";
import { useRef, useState } from "react";
import "../ui/theme/css/toast.css";
import {
  darkMode,
  items,
  previewStyleButton,
  toggleFullScreen,
} from "../ui/toast/toolbarItems";
import widgetRules from "../ui/toast/widgetRules";

// core
import "@toast-ui/editor/dist/theme/toastui-editor-dark.css";
import "@toast-ui/editor/dist/toastui-editor.css";

// react-editor
import uml from "@toast-ui/editor-plugin-uml";
import { Editor } from "@toast-ui/react-editor";

// editor-plugin-color-syntax
import colorSyntax from "@toast-ui/editor-plugin-color-syntax";
import "@toast-ui/editor-plugin-color-syntax/dist/toastui-editor-plugin-color-syntax.css";
import "tui-color-picker/dist/tui-color-picker.css";

// toastui-chart
import "@toast-ui/chart/dist/toastui-chart.css";
import chart from "@toast-ui/editor-plugin-chart";

// merged-cell
import tableMergedCell from "@toast-ui/editor-plugin-table-merged-cell";
import "@toast-ui/editor-plugin-table-merged-cell/dist/toastui-editor-plugin-table-merged-cell.css";

import Prism from "prismjs";
//https://github.com/PrismJS/prism-themes/tree/master/themes
// import "prismjs/themes/prism.css";
import "prismjs/themes/prism-coy.css";

// https://github.com/PrismJS/prism/tree/gh-pages/components
import codeSyntaxHighlight from "@toast-ui/editor-plugin-code-syntax-highlight";
import "@toast-ui/editor-plugin-code-syntax-highlight/dist/toastui-editor-plugin-code-syntax-highlight.css";
import "prismjs/components/prism-c.js";
import "prismjs/components/prism-cpp.js";
import "prismjs/components/prism-java.js";
import "prismjs/components/prism-javascript.min.js";
import "prismjs/components/prism-jsx.js";
import "prismjs/components/prism-python.js";

// Route Registration
export const Route = new FileRoute("/toast").createRoute({
  component: Toast,
});

function Toast() {
  const editorRef = useRef(null);
  const [previewStyle, setPreviewStyle] = useState<any>("vertical");
  let previewStyleTab = false;

  const toggleDarkMode = () => {
    let el = document.getElementsByClassName("toastui-editor-defaultUI")[0];
    if (el.classList.contains("toastui-editor-dark"))
      el.classList.remove("toastui-editor-dark");
    else el.classList.add("toastui-editor-dark");
  };

  const togglePreviewStyle = () => {
    setPreviewStyle(previewStyleTab ? "vertical" : "tab");
    previewStyleTab = !previewStyleTab;
  };

  const umlOptions = {
    rendererURL: "https://www.plantuml.com/plantuml/svg/",
  };

  const handleExportPDF = () => {
    const editorInstance = editorRef.current.getInstance();

    if (editorInstance) {
      const contentElement = editorInstance.el;

      // Log to see if contentElement is properly retrieved
      console.log("Content Element:", contentElement);

      // Use html2canvas to capture the editor content as an image
      html2canvas(contentElement).then((canvas) => {
        // Create a new jsPDF instance
        const pdfDoc = new jsPDF("p", "mm", "a4");

        // Convert the canvas to a data URL
        const imgData = canvas.toDataURL("image/png");

        // Add the image to the PDF
        pdfDoc.addImage(imgData, "PNG", 10, 10, 190, 250);

        // Save the PDF
        pdfDoc.save("toast_editor_content.pdf");
      });
    }
  };

  return (
    <>
      <button onClick={handleExportPDF}>Export to PDF</button>
      <br />
      <br />
      <Editor
        initialValue=""
        ref={editorRef}
        previewStyle={previewStyle}
        height="400px"
        initialEditType="markdown"
        useCommandShortcut={true}
        theme={"light"}
        widgetRules={widgetRules}
        toolbarItems={[
          ...items,
          [
            {
              name: "fullScreen",
              el: toggleFullScreen(editorRef),
              tooltip: "Full Screen",
            },
            {
              name: "previewStyle",
              el: previewStyleButton(togglePreviewStyle),
              tooltip: "Preview Markdown",
            },
            {
              name: "darkMode",
              el: darkMode(toggleDarkMode),
              tooltip: "Preview Style",
            },
          ],
          ["scrollSync"],
        ]}
        plugins={[
          [uml, umlOptions],
          colorSyntax,
          chart,
          tableMergedCell,
          [codeSyntaxHighlight, { highlighter: Prism }],
        ]}
      />
    </>
  );
}

export default Toast;
